<?php

use App\Reserve;
use App\Barber;
use App\ReserveService;
use App\TempUser;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/v1/TempUser', 'v1\TempUserController', [
    'except' => ['create,edit']]);

Route::resource('/v1/User', 'v1\UserController', [
        'except' => ['create,edit']]
);

Route::resource('/v1/Reserve', 'v1\ReserveController', [
        'except' => ['create,edit']]
);


Route::resource('/v1/Barber', 'v1\BarberController', [
        'except' => ['create,edit']]
);


Route::get('/v1/FindWithQrCode', 'v1\BarberShopController@FindWithQrCode', [
        'except' => ['create,edit']]
);

Route::get('/v1/MyBarbers', 'v1\BarberController@GetMyBarbers', [
        'except' => ['create,edit']]
);

Route::get('/v1/getBarberInformation', 'v1\BarberController@getBarberInformation', [
        'except' => ['create,edit']]
);


Route::get('/v1/MyReserves', 'v1\ReserveController@index', [
        'except' => ['create,edit']]
);

Route::get('/v1/MyReserves/{id}', 'v1\ReserveController@show', [
        'except' => ['create,edit']]
);

Route::get('/v1/getBarberService', 'v1\BarberServiceController@getBarberService', [
        'except' => ['create,edit']]
);

Route::post('/v1/BarberService', 'v1\BarberServiceController@store', [
        'except' => ['create,edit']]
);

Route::get('/v1/BarberRates', 'v1\BarberController@BarberRates', [
        'except' => ['create,edit']]
);

Route::get('/v1/UserBarberShops/{id}', function ($id) {

}
);

//Route::get('/v1/BarberReserveList/{id}', 'v1\BarberShopController@ShowBarberReserveListById', [
//        'except' => ['create,edit']]
//);


Route::get('/v1/BarberReserveList/{id}', function ($id) {
    //Todo

    //$mytime = Carbon\Carbon::now()->format('Y-m-d');
    $date = Carbon::now()->format('Y-m-d');
    $barbershop_id = Reserve::where('barber_shop', $id)->select('id', 'start_time', 'date', 'time_len', 'is_active')->get();
    if (empty($barbershop_id)) {
        // not found
        return '0';
    } else if (!empty($barbershop_id)) {
        //$barber_id = Barber::where('id', $id)->select('id')->get()->first();
        /// رزرو هایی که آی دی آزایشگاهش  برابر پبا آی دی که اومده $id
        /// و روزشم میاد که طبق اون خروجی میدیم.
        return response()->json(['message' => $barbershop_id], 200);
    } else {
        // we have input but it is invalid or something else.
        // i guess this section never run because always we have or not
        return '-1';
    }
}
);

Route::get('v1/login', 'v1\UserController@login');

Route::post('v1/CheckForActivationCode', 'v1\TempUserController@CheckForActivationCode');
Route::get('v1/test', function () {


    $id = 2;
    $reserveService = ReserveService::all();
    return $reserveService;

//
//    echo CarbonInterval::days($days)->hours($hours)->minutes($minutes)->forHumans();

    // تایم درخواست فعلیش رو با متد checkforactiva بررسی می کنیم
    // کد هم برابر باشه یعنی چک کنیم.
    // اگه تایمش باقی مونده بود true
    // نبود false
});

