<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarberService extends Model
{
    protected $table = 'barbersservice';
    public $timestamps = false;
}
