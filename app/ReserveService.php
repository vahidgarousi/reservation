<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveService extends Model
{
    protected $table = 'reserveservices';
    public $timestamps = false;
}
