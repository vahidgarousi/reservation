<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShop extends Model
{
    protected $table = 'usershops';
    public $timestamps = false;
}
