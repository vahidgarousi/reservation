<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempUser extends Model
{
    protected $table ="tempusers";
    public $timestamps = false;
}
