<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveDesc extends Model
{
    protected $table = 'reservedesc';
    public $timestamps = false;
}
