<?php

namespace App\Http\Controllers\v1;

use App\Services\v1\BarberServiceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarberServiceController extends Controller
{


    private $barberService;

    /**
     * BarberServiceController constructor.
     * @param BarberServiceService $service
     */
    public function __construct(BarberServiceService $service)
    {
        $this->barberService = $service;
    }

    protected $barber;

    public function getBarberService()
    {
        // call service
        $barberService = $this->barberService->getBarberService();
        return $barberService;
        // return data
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // call service
        $validation = $this->getValidationFactory()->make($request->all(), [
            'image' => 'required | mimes:jpeg,png'
        ]);
//        if ($validation->fails()) {
//            return response()->json(['message' => 'Invalid data!'], 400);
//        }
        $barberService = $this->barberService->addService($request);
        return $barberService;
        // return data
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
