<?php

namespace App\Http\Controllers\v1;

use App\Services\v1\UserService;
use App\TempUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $user;

    public function __construct(UserService $service)
    {
        $this->user = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Call Service
        return response()->json([$this->user->getUsers()], 200);
        //Return Data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function store(Request $request)
    {
        // Call Service
        $user = $this->user->createUser($request);
        if ($user == '0') {
            return response()->json(['message' => 'exist' . $user]);
        } else {
            return response()->json(['message' => $user]);
        }
        //Return data;
    }

    public function checkTimeForCreateUser()
    {
        $this->CheckForActivationCode();
    }


    public function CheckForActivationCode()
    {
        //TODO
        $phone_number = request()->input('phonenumber');
        $activation_code = request()->input('activation_code');
        $temp_user = TempUser::where('phonenumber', $phone_number)->get()->first();
        $requestTimeIsValid = $this->checkTimeForActivationCode($temp_user->time);
        //return $requestTimeIsValid;
        if ($requestTimeIsValid == 'true') {
            $requestActivationCodeIsTrue = $this->checkCodeForCreatingUser($temp_user, $activation_code);
            if ($requestActivationCodeIsTrue == 'true') {
                $token = $this->createToken($temp_user->id);
                return response()->json(['message' => [
                    'user' => $temp_user,
                    'token_time' => $token->expire_time
                ]], 400);
            }
            return response()->json(['code is incorrect' => $temp_user->code], 404);
        }
        return response()->json(['time is incorrect' => $temp_user], 404);
        //return $phone_number . ' ' . $activation_code . '/n' . $temp_user;
    }


    public function checkCodeForCreatingUser($temp_user, $activation_code)
    {
        if ($temp_user->code == $activation_code) {
            return 'true';
        }
        return 'false';
    }

    public function checkTimeForActivationCode($user_time)
    {
        $current_time = Carbon::now()->timestamp;
        $difference = $current_time - $user_time;
        $minute = $difference / 60;
        if ($minute >= 50) {
            return 'true';
        }
        return 'false';
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //call service
        $data = $this->user->getUser($id);
        return response()->json($data);
        //return  data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //  call service
        $user = $this->user->updateUser($request, $id);
        //dd($request->all());
        //return $id  .   '   '  . $request->name . '  ' . $request->family;
        if ($user == '0') {
            return response()->json(['message' => 'error']);
        } else if ($user == '1') {
            return response()->json(['message' => $user]);
        }
        // return data
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }

    public function login()
    {
        $userCanLogin = $this->user->login();
        return $userCanLogin;
    }
}
