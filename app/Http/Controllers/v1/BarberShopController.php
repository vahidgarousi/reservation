<?php

namespace App\Http\Controllers\v1;

use App\BarberShop;
use App\City;
use App\Image;
use App\QrCode;
use App\TempUser;
use Illuminate\Database\Eloquent\ModelNotFoundException;

//use Illuminate\Http\Request;

//use Illuminate\Http\Request;
use Request;
use phpDocumentor\Reflection\Types\Integer;
//use Request;
use App\Http\Controllers\Controller;
use App\Services\v1\TempUserService;
use Mockery\Exception;


class BarberShopController extends Controller
{
    /**
     * BarberShopController constructor.
     */
    protected $user;

    public function __construct(TempUserService $service)
    {
        $this->user = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //call service


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //call service

        //return  data
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param $code
     * @internal param int $id
     */
    public function update(Request $request)
    {
        //return $code;
        //call Service

        //return data
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function CheckForActivationCode()
    {
        //$phonenumber =
        return 'hello';
    }

    public function FindWithQrCode()
    {
        //TODO get QR code from barbershop and check it;
        $str_QrCode = Request::header('QrCode');

        $qr_code = QrCode::where('string', $str_QrCode)->get()->first();

        if (empty($qr_code)) {
            // not found
            return '0';
        } else if (!empty($qr_code)) {
            $barbershop = BarberShop::where('id', $qr_code->barber_shop)->get()->first();

            $avatar = Image::where('id', $barbershop->avatar)->get()->first();
            // dd($barbershop);
            $city = City::where('id', $barbershop->city)->get()->first();
            $data = [
                'name' => $barbershop->name,
                'address' => $barbershop->address,
                'avatar' => 'http://avida.ir/images/' . $avatar->name . '.jpg', // if we
                'city' => $city->name
            ];
            return $data;
            // Todo return barber shop information

        } else {
            // we have input but it is invalid or something else.
            // i guess this section never run because always we have qrcode or not
            return '-1';
        }

    }

}
