<?php

namespace App\Http\Controllers\v1;

use App\TempUser;
use App\Token;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//use Illuminate\Http\Request;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;
//use Request;
use App\Http\Controllers\Controller;
use App\Services\v1\TempUserService;
use Mockery\Exception;


class TempUserController extends Controller
{
    /**
     * TempUserController constructor.
     */
    protected $user;

    public function __construct(TempUserService $service)
    {
        $this->user = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //call service
        $accebility = $this->checkToken();
        if ($accebility == '0') {
            return response()->json(['message' => 'invalid data!'], 400);
        } else if ($accebility == '-2') {
            return response()->json(['message' => 'do not match!'], 400);
        } else if ($accebility == '-1') {
            return response()->json(['message' => 'time expired!'], 400);
        } else {
            if ($accebility['accessibility'] == 4) {
                $data = [
                    'user' => $accebility['user'],
                    'accessibility' => $accebility['accessibility']
                ];
                return response()->json($data);
            } else {
                //Todo Handle response
                return response()->json(['message' => 'access denied'], 404);
            }
        }
    }
    /*    public function checkToken()
        {
            $str_token = Request::header('Authorization');
            $user = Token::where('token', $str_token)->get();
            $user_id = $user->id;
            if ($user->isEmpty()) {
                return '0';
            } else {
                $id = $user->get('user');
                $user_type = User::where('id', $id)->get();
                //dd($user_type);
                //$user_accebility = Type::where('id', $user_type)->select('accebility')->get();
                return $user;
            }
        }*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //$phoneNumber = $request->input('phonenumber');
        try {
            $tempUser = $this->user->createTempUser($request);
            return response()->json($tempUser, 201);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //call service
        $data = $this->user->getUser($id);
        return response()->json($data);
        //return  data
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param $code
     * @internal param int $id
     */
    public function update(Request $request)
    {
        //return $code;
        //call Service
        try {
            $tempUser = $this->user->updateTempUser($request);
            return response()->json($tempUser, 200);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
        //return data
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function CheckForActivationCode()
    {
        //TODO
        $phone_number = request()->input('phonenumber');
        $activation_code = request()->input('activation_code');
        $temp_user = TempUser::where('phonenumber', $phone_number)->get()->first();
        $requestTimeIsValid = $this->checkTimeForActivationCode($temp_user->time);
        //return $requestTimeIsValid;
        if ($requestTimeIsValid == 'true') {
            $requestActivationCodeIsTrue = $this->checkCodeForCreatingUser($temp_user, $activation_code);
            if ($requestActivationCodeIsTrue == 'true') {
                $token = $this->createToken($temp_user->id);
                return response()->json(['message' => [
                    'user' => $temp_user,
                    'token_time' => $token->expire_time
                ]], 400);
            }
            return response()->json(['code is incorrect ' => $temp_user->code], 400);
        }
        return response()->json(['time is incorrect' => $temp_user], 400);
        //return $phone_number . ' ' . $activation_code . '/n' . $temp_user;
    }

    public function checkTimeForActivationCode($user_time)
    {
        $current_time = Carbon::now()->timestamp;
        $difference = $current_time - $user_time;
        $minute = $difference / 60;
        if ($minute <= 50) {
            return 'true';
        }
        return 'false';
    }

    public function createToken($user_id)
    {
        $token = new Token();
        $token->user = $user_id;
        $token_expire_time = Carbon::now()->timestamp;
        $token->expire_time = $token_expire_time;
        $token->token = 'ssssssssssssvfsboerhjboeriubheobhreoberbreber' . $user_id . Carbon::now()->timestamp;
        $token->save();
        return $token;
    }

    public function checkCodeForCreatingUser($temp_user, $activation_code)
    {
        if ($temp_user->code == $activation_code) {
            return 'true';
        }
        return 'false';
    }

    public function test()
    {
        $value = '1518890464';
        $dt = Carbon::now();
        $days = $dt->diffInDays($dt->copy()->addSeconds($value));
        $hours = $dt->diffInHours($dt->copy()->addSeconds($value)->subDays($days));
        $minutes = $dt->diffInMinutes($dt->copy()->addSeconds($value)->subHours($hours));
        echo CarbonInterval::days($days)->hours($hours)->minutes($minutes)->forHumans();
    }
}
