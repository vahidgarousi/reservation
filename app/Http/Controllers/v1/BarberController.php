<?php

namespace App\Http\Controllers\v1;

use App\Services\v1\BarberService;

use Illuminate\Http\Request;
//use Request;
use App\Http\Controllers\Controller;


class BarberController extends Controller
{
    /**
     * BarberController constructor.
     */
    protected $barber;

    public function __construct(BarberService $service)
    {
        $this->barber = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //call service


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //call service
        $barber = $this->barber->getBarberInformation();
        return $barber;
        //return  data
    }

    public function BarberRates()
    {
        //call service
        $barber = $this->barber->getBarberRates();
        return $barber;
        //return  data
    }

    public function getMyBarbers()
    {
        //call service
        $barbers = $this->barber->getMyBarbers();
        return $barbers;
        //return  data
    }

    public function getBarberInformation()
    {
        //call service
        $barber = $this->barber->getBarberInformation();
        return $barber;
        //return  data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param $code
     * @internal param int $id
     */
    public function update(Request $request)
    {
        //return $code;
        //call Service

        //return data
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function CheckForActivationCode()
    {
        //$phonenumber =
        return 'hello';
    }


    public function ShowBarberReserveListById($id)
    {
        return 'ShowBarberReserveListById' . $id;
    }

}
