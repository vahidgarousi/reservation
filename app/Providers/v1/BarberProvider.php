<?php

namespace App\Providers\v1;

use App\Services\v1\BarberService;
use Illuminate\Support\ServiceProvider;

class BarberProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BarberService::class, function ($app) {
            return new BarberService();
        });
    }
}
