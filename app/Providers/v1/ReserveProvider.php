<?php

namespace App\Providers\v1;

use App\ReserveService;
use Illuminate\Support\ServiceProvider;

class ReserveProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ReserveService::class, function ($app) {
            return new ReserveService();
        });
    }
}
