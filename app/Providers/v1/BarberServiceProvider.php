<?php

namespace App\Providers\v1;

use App\BarberService;
use App\Services\v1\BarberServiceService;
use Illuminate\Support\ServiceProvider;

class BarberServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BarberServiceService::class, function ($app) {
            return new BarberServiceService();
        });
    }
}
