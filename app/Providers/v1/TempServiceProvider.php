<?php

namespace App\Providers\v1;

use Illuminate\Support\ServiceProvider;
use app\Services\v1\TempUserService;

class TempServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TempUserService::class, function ($app) {
            return new TempUserService();
        });
    }
}
