<?php
/**
 * Created by PhpStorm.
 * User: Vahid
 * Date: 2/8/2018
 * Time: 7:54 AM
 */

namespace App\Services\v1;


use App\Barber;
use App\BarberShop;
use App\Image;
use App\Rate;
use App\Reserve;
use App\Token;
use App\User;
use App\UserShop;
use Carbon\Carbon;
use function Psy\sh;
use Request;
use function Symfony\Component\VarDumper\Tests\Fixtures\bar;

class BarberService
{

    private $token_expire_time = 36000;

    public function getBarberInformation()
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            $barberInformation = User::where('userid', $userIdInTokenModel)->get()->first();
            if ($barberInformation instanceof User) {
                return $barberInformation;
            } else {
                return '0';
            }
        }
    }

    public function getMyBarbers()
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            //   $user = User::where('userid', $userIdInTokenModel)->select('userid')->get()->first();
            //  $userBarberShops = UserShop::where('users', $userIdInTokenModel)->get();

            $userShop = UserShop::where('users', $userIdInTokenModel)->get();
            $userShopArray = array();
            $barberShopArray = array();
            $barberArray = array();
            $userArray = array();
            $userAvatarArray = array();
            foreach ($userShop as $userShopItem) {
                $barberShop = BarberShop::where('id', $userShopItem->id)->get();
                foreach ($barberShop as $barberShopItem) {
                    $barberShopArray[] = $barberShopItem;
                    $barber = Barber::where('barber_shop', $barberShopItem->id)->get();
                    foreach ($barber as $barberItem) {
                        $barberArray[] = $barberItem;
                        $user = User::where('userid', $barberItem->user)->get();
                        foreach ($user as $userItem) {
                            $userArray[] = $userItem;
                            $image = Image::where('id', $userItem->image)->get();
                            foreach ($image as $userImage) {
                                $userAvatarArray[] = 'http://avida.ir/' . $userImage->name;
                            }
                        }
                    }
                    $userShopArray[] = $userShopItem;
                }
            }
        }
        $data = [
            'usershops' => $userShopArray,
            'barbershop' => $barberShopArray,
            'barber' => $barberArray,
            'user' => $userArray,
            'image' => $userAvatarArray,
        ];
        return $data;

//            //   $barberShopId = Barber::where('user',$userIdInTokenModel)->get()->first();
//            $barberInformation = User::where('userid', $userIdInTokenModel)->get();
//            $data = array();
//            foreach ($barberInformation as $information) {
//                $entry = [
//                    'name' => $information->name,
//                    'family' => $information->family,
//                    'image' => $information->image,
//                ];
//                $data[] = $entry;
//            }
//
//            return $barberShop;
    }


    public
    function getBarberRates()
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            $barber = User::where('userid', $userIdInTokenModel)->select('userid')->get()->first();
            $barberId = $barber->userid;
            if ($barber instanceof User) {
                $rates = Rate::where('barber', $barberId)->get();
                return $rates;
            } else {
                return '0';
            }
        }
    }

    public function checkToken()
    {
        $str_token = Request::header('Authorization');
        //Todo handle errors

        $token = Token::where('token', $str_token)->get()->first();
        //return $token
        if ($token->expire_time >= $this->token_expire_time) {
            return $token;
        }
        return '0';
    }

}