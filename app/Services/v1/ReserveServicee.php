<?php
/**
 * Created by PhpStorm.
 * User: Vahid
 * Date: 2/8/2018
 * Time: 7:54 AM
 */

namespace App\Services\v1;

use App\Barber;
use App\BarberShop;
use App\Image;
use App\Reserve;
use App\ReserveDesc;
use App\Service;
use App\ReserveService;
use App\Token;
use App\User;
use Carbon\Carbon;
use DateTime;
use Request;

class ReserveServicee
{

    protected $supportedIncludes = [
        'detail' => 'detail'
    ];
    private $token_expire_time = 36000;

    public function addReserve($req)
    {
        $data = json_decode($req, true);
        $body = $data['body'];
        $shopId = $body['barbershop']; // use
        //$barbershopId = BarberShop::where('shop_id',$shopId)->select('id')->get()->first();
        $barbershop = BarberShop::where('shop_id', $shopId)->get()->first();
        $barbershopId = $barbershop['id'];
        $user = Barber::where('barber_shop', $barbershopId)->get()->first();
        $userId = $user['user']; // use

        $start_time = $body['time']; // use

        $serviceJson = $body['service']; // use
        $serviceTotalTime = 0;
        $services ['service'] = $serviceJson;
        $servicesItems['service_id'] = array();
        foreach ($serviceJson as $serviceItem) {
            $serviceId = $serviceItem['service_id']; // use //// ****************************
            $service = Service::where('id', $serviceId)->get()->first();
            $serviceTimeLen = $service['time_len'];
            $serviceTotalTime += $serviceTimeLen;
        }
        $time_len = $serviceTotalTime; // use

        $date = date('Y-m-d'); //use

        $reserve = new Reserve();
        $reserve->barber_shop = $barbershopId;
        $reserve->user = $userId;
        $reserve->time_len = $time_len;
        $reserve->start_time = $start_time;
        $reserve->date = $date;
        if ($reserve->save()) {
            $reserveServices = new ReserveService();
            $reserveServices->reserves = $reserve->id;
            $reserveServices->services = json_encode($services);
            if ($reserveServices->save()) {
                $descriptionJson = $data['description'];
                $description = [];
                foreach ($descriptionJson as $descItem) {
                    $description['text'] = $descItem['text'];
                    $picDescription = $descItem['pic'];
                    $description['pic'] = $picDescription['file'];
                }
                $reserveDescription = new ReserveDesc();
                $reserveDescription->text = $description['text'];
                $reserveDescription->reserve = $reserve->id;
                $serviceImage = new Image();
                $serviceImage->name = $description['pic'];
                if ($serviceImage->save()) {
                    $reserveDescription->images = $serviceImage->id;
                    if ($reserveDescription->save()) {
                        return 'saved successfully';
                    } else {
                        return 'saved failed';
                    }
                }
            }
            return $reserve;
        }
        return 'some error';
//        // Todo compute time
//        // /// Todo and then store reserve
    }

    public function getMyReserves()
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            $parameters = request()->input();
            //return response()->json(['message' => $parameters]);
            if (empty($parameters)) {
                $reserves = Reserve::where('user', $userIdInTokenModel)->get();
                // return response()->json(['message' => $reserves]);
                foreach ($reserves as $reserve) {
                    if ($reserve instanceof Reserve) {
                        return $reserves;
                    }
                }
            }
            $withKeys = [];
            if (isset($parameters['include'])) {
                $includeParams = explode(',', $parameters['include']);
                $includes = array_intersect($this->supportedIncludes, $includeParams);
                $withKeys = array_keys($includes);
            }
            return $this->filterReserves(Reserve::where('user', $userIdInTokenModel)->get(), $withKeys);
            //return $parameters ;
        }
    }

    public function filterReserves($reserves, $keys = [])
    {
        $data = [];
        foreach ($reserves as $item) {
            $serviceArray = [];
            $id = $item->id;
            if (in_array('detail', $keys)) {
                $reserveDescription = ReserveDesc::where('reserve', $item->id)->get();
                foreach ($reserveDescription as $reserveDescriptionItem) {
                    if ($reserveDescriptionItem instanceof ReserveDesc) {
                        $reserveService = ReserveService::where('reserves', $id)->get()->first();
                        if ($reserveService instanceof ReserveService) {
                            $servicesJson = json_decode($reserveService->services);
                            foreach ($servicesJson as $s) {
                                foreach ($s as $value) {
                                    $serviceId = $value->service_id;
                                    $service = Service::where('id', $serviceId)->get();
                                    array_push($serviceArray, 'service', $service);
                                }
                            }
                            $entry['data'] = [
                                'reserve' => $item,
                                'description' => $reserveDescription,
                                'serviceDetail' => $serviceArray,
                            ];
                            $data[] = $entry;
                        }
                    } else {
                        return $reserveDescription;
                    }
                }
            }
        }
        return $data;
    }

    public function getMyReservesWithId($id, $parameters)
    {
        // Todo handle query mark in url
        $token = $this->checkToken();
        if ($token == '-1') {
            return response()->json(['message' => 'not found'], 404);
        } else if ($token == '0') {
            return response()->json(['message' => 'time expired'], 404);
        } else {
            if (empty($parameters)) {
                $reserves = Reserve::where('id', $id)->get();
                foreach ($reserves as $reserve) {
                    if ($reserve instanceof Reserve) {
                        return $reserves;
                    }
                }
            }

            $withKeys = [];
            if (isset($parameters['include'])) {
                $includeParams = explode(',', $parameters['include']);
                $includes = array_intersect($this->supportedIncludes, $includeParams);
                $withKeys = array_keys($includes);
            }
//            $reserves = Reserve::where('id', $id)->get();
//            foreach ($reserves as $reserve) {
//                if ($reserve instanceof Reserve) {
//                    return $this->filterReserves($reserve, $withKeys);
//                }
//            }
            return $this->filterReserves(Reserve::where('id', $id)->get(), $withKeys);
        }
        return response()->json(['message' => 'invalid data'], 404);
    }


    function checkToken()
    {
        $str_token = Request::header('Authorization');
        //Todo handle errors

        $token = Token::where('token', $str_token)->get()->first();
        //return $token
        if ($token instanceof Token) {
            if ($token->expire_time >= $this->token_expire_time) {
                return $token;
            } else {
                return '-1';
            }
        } else {
            return '0';
        }
    }
}