<?php
/**
 * Created by PhpStorm.
 * User: Vahid
 * Date: 2/8/2018
 * Time: 7:54 AM
 */

namespace App\Services\v1;

use App\UserAccount;
use http\Exception;
use Illuminate\Support\Str;
use App\Image;
use App\TempUser;
use App\Type;
use App\User;
use Carbon\Carbon;
use App\Token;
use Request;

class UserService
{
    private $token_expire_time = 36000;
    private $hashKey;

    public function getUsers()
    {
        if (User::all()->isEmpty()) {
            return response()->json(['message' => 'record not found'], 404);
        }
        return $this->filterUser(User::all());
        //return TempUser::all();
    }

    public function getUser($phoneNumber)
    {
        return $this->filterUserForCreatingUserResult(User::where('phonenumber', $phoneNumber)->get());
    }

    public function filterUserForCreatingUserResult($phoneNumber)
    {
        $user = User::where('phonenumber', $phoneNumber)->get()->first();

        var_dump($user);
//        $token = Token::where('user',$user->id)->get()->first();
        return $user;

    }

    protected function filterUser($users)
    {
        // فیلتر کردن این که اطلاعاتمون رو همشو ندیم مثلا آی دی رو ندادم پایین
        $data = [];

        //return $token;
        foreach ($users as $user) {
            $image = Image::find($user->image);
            $entry = [
                'user_id' => $user->userid,
                'href' => route('User.show', ['id' => $user->phonenumber])
            ];
            $data[] = $entry;
        }
        return $data;
    }


    public function CheckForActivation($time, $activation_code, $phone_number)
    {
        //return $time . ' ' . $activation_code. ' ' .$phone_number;
        $temp_user = TempUser::where('phonenumber', $phone_number)->get()->first();
        if (empty($temp_user)) {
            return $temp_user;
        } else {
            if (!empty($temp_user) && $temp_user->code == $activation_code && $temp_user->time <= $time) {
                //   return response()->json($tempUser);
                return 'true';
                //return $req->code;
//            return response()->json(['message'], 400);
            } else if ($activation_code != $temp_user->code) {
                return response()->json(['message' => 'Code is incorrect'], 404);
                // return false;
            } else if ($temp_user->time >= $time) {
                return response()->json(['message' <= 'Time invalid'], 404);
                //return false;
            } else {
                return response()->json(['message' => 'Not Found'], 404);
                //return false;
            }
        }

        // تایم درخواست فعلیش رو با متد checkforactiva بررسی می کنیم
        // کد هم برابر باشه یعنی چک کنیم.
        // اگه تایمش باقی مونده بود true
        // نبود false
    }


    public function createUser($request)
    {
        try {
            $canInsert = $this->CheckForActivationCode();
            if ($canInsert == 'true') {
                $username = $request->username;
                $password = $request->password;
                $user = $this->checkIfNotExitsCreate($request->phonenumber);
                if ($user == '0') {
                    $user = new User();
                    $user->userid = $this->generateCode();
                    $user->phonenumber = $request->phonenumber;
                    $user->save();
                    $user_account = new UserAccount();
                    $user_account->accebility = 1;
                    $user_account->user = $user->id;
                    $user_account->name = $username;
                    $user_account->password = $password;
                    $user_account->save();
                    $temp_user = TempUser::where('phonenumber', $request->phonenumber)->delete();
                    $data = [
                        'user_Account' => $user_account,
                        'userid' => $user->userid
                    ];
                    return $data;
                    //dd($request->all());
                    //   $token = $this->createToken($temp_user->id);
                } else {
                    return '0';
                }
            }
            return $canInsert;
        } catch (Exception $exception) {

        }
        // After we saved our user
        //  We send it to the output
    }

    public function updateUser($req, $id)
    {

        $name = $req->name;
        $family = $req->family;
        $user = User::where('userid', $id)->get()->first();
        if (empty($user)) {
            return '0';
        } else {
            $user->name = $name;
            $user->family = $family;
            $user->type = 1;
            $user->save();
            return '1';
        }
    }

    public function login()
    {
        $username = Request::header('username');
        $password = Request::header('password');

        $user = UserAccount::where('name', $username)->where('password', $password)->get()->first();

        //return $username . ' ' . $password . ' ' . $user;
        if($user instanceof  UserAccount) {
            return response()->json(['message' => $user]);
        }
        return response()->json(['message' => 'username & password was wrong' . $user]);
    }

    public function checkTimeForActivationCode($user_time)
    {
        $current_time = Carbon::now()->timestamp;
        $difference = $current_time - $user_time;
        $minute = $difference / 60;
        if ($minute <= 50) {
            return true;
        }
        return false;
    }

    public function checkCodeForCreatingUser($temp_user, $activation_code)
    {
        if ($temp_user->code == $activation_code) {
            return true;
        }
        return false;
    }


    public function CheckForActivationCode()
    {
        //TODO
        $phone_number = request()->input('phonenumber');
        $activation_code = request()->input('activation_code');
        $temp_user = TempUser::where('phonenumber', $phone_number)->get()->first();
        $requestTimeIsValid = $this->checkTimeForActivationCode($temp_user->time);
        if ($requestTimeIsValid == true) {
            $requestActivationCodeIsTrue = $this->checkCodeForCreatingUser($temp_user, $activation_code);
            if ($requestActivationCodeIsTrue == true) {
                return 'true';
            }
            return response()->json(['code is incorrect ' => $temp_user->code], 400);
            //  return 'false';
        }
        // return 'false';
        return response()->json(['time is incorrect' => $temp_user->time], 400);
        // return $phone_number . ' ' . $activation_code . '/n' . $temp_user;
    }

    public function generateTokenExpireTime()
    {
        return 36000;
    }

    public function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), $this->hashKey);
    }

    public function checkToken($user_token)
    {
        //$str_token = Request::header('Authorization');
        //Todo handle errors

        $token = Token::where('token', $user_token)->get()->first();
        //return $token;
        if ($token->expire_time <= $this->token_expire_time) {
            return 'true';
        }
        return 'false';
//        if (empty($str_token)) {
//            // return 'invalid data!';
//            return '0';
//        } else {
//            //Todo تایمش رد نشده باشه
//            $user = Token::where('token', $str_token)->get()->first();
//            if (!empty($user)) {
//                if ($user->expire_time <= $this->token_expire_time) {
//                    // return 'time expired';
//                    return '-1';
//                } else {
//                    $user = User::where('id', $user->user)->get()->first();
//                    $user_accebility = Type::where('id', $user->type)->select('accebility')->get()->first();
//                    $data = [
//                        'user' => $user,
//                    ];
//                    return $user;
//                }
//            } else {
//                //   return 'do not match';
//                return '-2';
//            }
//        }
    }

    protected
    function generateCode()
    {
        $code = random_int(0, 99999999);
        return $code;
    }

    public function checkIfNotExitsCreate($phone_number)
    {
        $user = User::where('phonenumber', $phone_number)->get()->first();
        if (empty($user)) {
            return '0';
        }
        $user = new User();
    }

}