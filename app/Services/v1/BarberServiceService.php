<?php
/**
 * Created by PhpStorm.
 * User: Vahid
 * Date: 2/8/2018
 * Time: 7:54 AM
 */

namespace App\Services\v1;


use App\Barber;
use App\Image;
use App\Rate;
use App\Reserve;
use App\Service;
use App\Token;
use App\User;
use Carbon\Carbon;
use App\BarberService;
use Request;
use function Symfony\Component\VarDumper\Tests\Fixtures\bar;

class BarberServiceService
{

    private $token_expire_time = 36000;
    private $supportedIncludes = [
        'detail' => 'detail'
    ];


    public function getBarberService()
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            $parameters = request()->input();
            if (empty($parameters)) {
                $services = Service::where('barber', $userIdInTokenModel)->get();
                $data = [];
                foreach ($services as $item) {
                    if ($item instanceof Service) {
//                    $barberServices = $item->service;
                        $barberService = BarberService::where('id', $item->services)->get()->first();
                        $image = Image::find($item->images);
                        $imageLink = 'http://avida.ir/images/' . $image->name;
                        $entry = [
                            'time_len' => $item->time_len,
                            'amount' => $item->amount * 1000,
                            'image' => $imageLink,
                            'name' => $barberService->name
                        ];
                        $data[] = $entry;
                    } else {
                        return '0';
                    }
                }
                return $data;
            }


            $withKeys = [];
            if (isset($parameters['include'])) {
                $includeParams = explode(',', $parameters['include']);
                $includes = array_intersect($this->supportedIncludes, $includeParams);
                $withKeys = array_keys($includes);
            }
            $services = Service::where('barber', $userIdInTokenModel)->get();

            return $this->filterService($services, $withKeys);
        }
    }

    public function filterService($services, $keys = [])
    {
        // TODO complete this section
        // get services info
        $data = [];
        foreach ($services as $item) {
            if (in_array('detail', $keys)) {
                $servImage = Image::find($item->images);
                $serviceImage = $servImage->name;
                $serviceDetail = [
                    'image' => 'http://avida.ir/images/' . $serviceImage,
                    'time_len' => $item->time_len,
                    'amount' => $item->amount,
                ];
                $data[] = $serviceDetail;
            }
        }
        return $data;
    }

    public function checkToken()
    {
        $str_token = Request::header('Authorization');
        //Todo handle errors

        $token = Token::where('token', $str_token)->get()->first();
        //return $token
        if ($token->expire_time >= $this->token_expire_time) {
            return $token;
        }
        return '0';
    }

    public function addService($request)
    {
        $token = $this->checkToken();
        if ($token == '0') {
            return response()->json(['message' => 'time expire']);
        } else {
            $userIdInTokenModel = $token->user;
            $user = User::where('userid', $userIdInTokenModel)->get()->first();
            if ($user->type == '2') {
                $service = new Service();
                $service->services = $request->services;
                $service->time_len = $request->time_len;
                $service->amount = $request->amount;
                $service->day_start = $request->day_start;
                $service->day_delay = $request->day_delay;
                $service->barber = $userIdInTokenModel;
                $service->save();
                $imageName = 'service' . $service->id . '.' . request()->file('image')->getClientOriginalExtension();
                request()->file('image')->move(public_path('images'), $imageName);
                $image = new Image();
                $image->name = $imageName;
                $image->save();
                $service->images = $image->id;
                $service->save();
                return $service;
            } else {
                return response()->json(['message' => 'access denied'], 401);
            }
        }
    }

}