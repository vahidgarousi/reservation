<?php
/**
 * Created by PhpStorm.
 * User: Vahid
 * Date: 2/8/2018
 * Time: 7:54 AM
 */

namespace App\Services\v1;

use App\TempUser;
use App\Token;
use App\Type;
use App\User;
use Carbon\Carbon;
use Request;
use Ramsey\Uuid\Generator\TimeGeneratorFactory;


class TempUserService
{

    public function getUsers()
    {
        return $this->filterUser(TempUser::all());
        //return TempUser::all();
    }

    protected $token_expire_time = 28800;

    public function getUser($phoneNumber)
    {
        //return $phoneNumber;
        return $this->filterUser(TempUser::where('phonenumber', $phoneNumber)->get());
    }

    protected function filterUser($users)
    {
        $data = [];
        foreach ($users as $user) {
            $entry = [
                'phoneNumber' => $user->phonenumber,
                'code' => $user->code,
                'time' => $user->time,
                'href' => route('TempUser.show', ['id' => $user->phonenumber])
            ];
            $data[] = $entry;
        }
        return $data;
    }

    public function createTempUser($req)
    {
        $phonenumber = $req->input('phonenumber');

        $user = TempUser::where('phonenumber', $phonenumber)->get();

        if ($user->isEmpty()) {
            $tempUser = new TempUser();
            $tempUser->phonenumber = $phonenumber;
            $tempUser->time = $this->getCurrentTime();
            $tempUser->code = $this->generateCode();
            $tempUser->save();
        } else {
            // return user existing time in second
            return 'exist';
        }
        // return user existing time in second
        return $this->filterUser([$tempUser]);
        // سه حالت داریم یا جدیده که تایم میدیم پایین
        // یا تایمش مونده باقیمانده شو میدیم پایین
        // یا ریست می کنیم.*/
    }

    public function updateTempUser($req)
    {
        $phonenumber = $req->input('phonenumber');

        $tempUser = TempUser::where('phonenumber', $phonenumber)->firstOrFail();

        if (!empty($tempUser) && $tempUser->code == $req->code && $tempUser->time >= $req->time) {
//            return response()->json($tempUser);
            return response()->json(['message' => 'Found and code is: ' . $req->code], 400);
            //return $req->code;
//            return response()->json(['message'], 400);
        } else if ($req->code != $tempUser->code) {
            return response()->json(['message' => 'Code is incorrect'], 400);
        } else if ($tempUser->time <= $req->time) {
            return response()->json(['message' => 'Time invalid'], 400);
        } else {
            return response()->json(['message' => 'Not Found'], 400);
        }
    }


    public function checkToken()
    {
        $str_token = Request::header('Authorization');
        //Todo handle errors
        if (empty($str_token)) {
            // return 'invalid data!';
            return '0';
        } else {
            //Todo تایمش رد نشده باشه
            $user = Token::where('token', $str_token)->get()->first();
            if (!empty($user)) {
                if ($user->expire_time <= $this->token_expire_time) {
                    // return 'time expired';
                    return '-1';
                } else {
                    $user = User::where('id', $user->user)->get()->first();
                    $user_accebility = Type::where('id', $user->type)->select('accebility')->get()->first();
                    $data = [
                        'user' => $user,
                        'accessibility' => $user_accebility->accebility
                    ];
                    return $data;
                }
            } else {
                //   return 'do not match';
                return '-2';
            }
        }
    }

    /**
     * @return int
     */
    public function getCurrentTime()
    {
        $time = Carbon::now()->timestamp;
        return $time;
    }

    public function CheckForActivation($time, $activation_code, $phone_number)
    {
        $temp_user = TempUser::where('phonenumber', $phone_number)->firstOrFail();
        if (empty($temp_user)) {
            return 'empty';
        } else {
            if (!empty($tempUser) && $tempUser->code == $activation_code && $tempUser->time >= $time) {
//            return response()->json($tempUser);
                return response()->json(['message' => 'Found and code is: ' . $req->code], 400);
                //return $req->code;
//            return response()->json(['message'], 400);
            } else if ($activation_code != $tempUser->code) {
                return response()->json(['message' => 'Code is incorrect'], 400);
            } else if ($tempUser->time <= $time) {
                return response()->json(['message' => 'Time invalid'], 400);
            } else {
                return response()->json(['message' => 'Not Found'], 400);
            }
            return 'not is empty';
        }

        // تایم درخواست فعلیش رو با متد checkforactiva بررسی می کنیم
        // کد هم برابر باشه یعنی چک کنیم.
        // اگه تایمش باقی مونده بود true
        // نبود false
    }

    protected function generateCode()
    {
        $code = random_int(1000, 9999);
        return $code;
    }
}