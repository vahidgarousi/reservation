<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barber extends Model
{
    protected $table = "barber";

    public function userAccount()
    {
        return $this->belongsTo('App\User');
    }

    public function barberShop()
    {
        return $this->belongsTo('App\BarberShop');
    }

    public function service()
    {
        return $this->hasMany('App\Service');
    }

    public function rate()
    {
        return $this->hasMany('App\Rate');
    }


}
