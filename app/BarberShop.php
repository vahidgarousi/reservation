<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarberShop extends Model
{
    protected $table = "barbershop";
    public $timestamps = false;
}
